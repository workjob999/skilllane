import React from "react";
import {
    Nav,
    Navbar,
    NavDropdown,
    Dropdown,
    Form,
    FormControl,
    Button,
    Tabs,
    Tab
} from "react-bootstrap";

const DetailsCourse = () => (
    <div>
        <Tabs defaultActiveKey="1" id="">
            <Tab eventKey="1" title="รายละเอียด">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and
                scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap
                into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                software like Aldus PageMaker including versions of Lorem Ipsum.
            </Tab>
            <Tab eventKey="2" title="วิธีการชำระเงิน" tabClassName="d-sm-block d-none">
                TEST
            </Tab>
            <Tab eventKey="3" title="ห้องสนทนา">
                TEST
            </Tab>
            <Tab eventKey="4" title="รีวิว">
                TEST
            </Tab>
        </Tabs>
    </div>
);

export default DetailsCourse;
