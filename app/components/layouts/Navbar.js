import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'

import { Form, FormControl, InputGroup, Nav, Navbar, NavDropdown } from "react-bootstrap"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from "@fortawesome/free-solid-svg-icons";

const mainNav = () => (
    <Navbar bg="white" expand="lg" className="py-0">
        <NavDropdown title="Dropdown" id="basic-nav-dropdown" className="d-block d-md-none">
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
            <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
            <NavDropdown.Divider/>
            <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
        </NavDropdown>
        <Navbar.Brand href="#home"><img
            src="https://s3-ap-southeast-1.amazonaws.com/skilllane/images/Logo/logo.png"
            className="logo"/></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
        <Navbar.Collapse id="basic-navbar-nav">

            <Form inline>
                <InputGroup className="group-search">
                    <FormControl
                        type="text"
                        placeholder="ค้นหาคอร์สเรียน ..."
                    />

                    <InputGroup.Prepend>
                        <InputGroup.Text id="basic-addon1">
                            <FontAwesomeIcon icon={faSearch} style={{ color: '#dddddd' }} />
                        </InputGroup.Text>
                    </InputGroup.Prepend>
                </InputGroup>
            </Form>

            <Nav className="ml-auto">
                <Nav.Link href="#home">คอร์สเรียนทั้งหมด
                </Nav.Link>
                <Nav.Link href="#link">TUXSA</Nav.Link>
                <Nav.Link href="#link">สำหรับองค์กร</Nav.Link>
                <Nav.Link href="#link">สอนกับเรา</Nav.Link>
                <Nav.Link>|</Nav.Link>
                <Nav.Link href="#link">เข้าสู่ระบบ</Nav.Link>
                <Nav.Link href="#link">สมัครสมาชิก</Nav.Link>


            </Nav>
        </Navbar.Collapse>
    </Navbar>
)

export default mainNav