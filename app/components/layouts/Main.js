import React from 'react';
import Header from './Header'

const Main = (props) => {
    return (
        <>
            <Header />

            <div>
                {props.children}
            </div>
        </>
    )
}

export default Main