import Head from "next/head";
import React from "react";
import Navbar from "./Navbar";

const Header = () => (
    <>
        <Head>
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
            <title>คอร์สปริญญาโทออนไลน์ Design Thinking กระบวนการคิดเชิงออกแบบ | SkillLane</title>

            <link rel="icon" type="image/png"
                  href="https://skilllane.s3-ap-southeast-1.amazonaws.com/favicon/16_16px.png" sizes="16x16"/>
            <link rel="icon" type="image/png"
                  href="https://skilllane.s3-ap-southeast-1.amazonaws.com/favicon/32_32px.png" sizes="32x32"/>
            <link rel="icon" type="image/png"
                  href="https://skilllane.s3-ap-southeast-1.amazonaws.com/favicon/96_96px.png" sizes="96x96"/>

            <link rel="apple-touch-icon"
                  href="https://skilllane-b2b-public.s3-ap-southeast-1.amazonaws.com/images/icons/120x120.png"/>
            <link rel="apple-touch-icon" sizes="152x152"
                  href="https://skilllane-b2b-public.s3-ap-southeast-1.amazonaws.com/images/icons/152x152.png"/>
            <link rel="apple-touch-icon" sizes="167x167"
                  href="https://skilllane-b2b-public.s3-ap-southeast-1.amazonaws.com/images/icons/167x167.png"/>
            <link rel="apple-touch-icon" sizes="180x180"
                  href="https://skilllane-b2b-public.s3-ap-southeast-1.amazonaws.com/images/icons/180x180.png"/>
        </Head>

        <header>
            <Navbar/>
        </header>
    </>
)

export default Header