import React from "react";

import { Row, Col, Button } from "react-bootstrap";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlayCircle, faCheckSquare } from "@fortawesome/free-regular-svg-icons";
import { faFileDownload } from "@fortawesome/free-solid-svg-icons";

const session = (props) => {
  const { data } = props;
  return (
    <ul className="courselist">
      <div className="title px-2">{data.title}</div>
      {data.session.map((list) => {
        return (
          <li className={`${list.hasExample ? "preview" : ""}`}>
            <Row>
              <Col md={7}>{list.title}</Col>
              <Col md={3} className="text-right pr-0">
                {list.hasExample && (
                  <span className="label label-success">ดูตัวอย่างฟรี</span>
                )}

                {list.hasDownload && (
                  <FontAwesomeIcon icon={faFileDownload} className="mr-2" />
                )}

                {list.hasTest && (
                    <FontAwesomeIcon icon={faCheckSquare} />
                )}

                {!list.hasTest && (
                    <FontAwesomeIcon icon={faPlayCircle} />
                )}
              </Col>
              <Col md={2} className="text-right">
                {list.time}
              </Col>
            </Row>
          </li>
        );
      })}
    </ul>
  );
};

export default session;
