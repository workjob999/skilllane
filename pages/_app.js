import 'bootstrap/dist/css/bootstrap.min.css'
import '../app/styles/globals.css'
import '../app/styles/style.css'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
