import React from "react";
import Link from "next/link";

import Layout from "../app/components/layouts/Main";
import DetailsCourse from "../app/components/course";
import Session from "../app/components/session";

import { Container, Row, Col, Button } from "react-bootstrap";
import StarRatings from "react-star-ratings";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faShareAlt,
  faCheck,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";

const sessionCourse = [
  {
    title: "บทนำ",
    session: [
      {
        title: "แนะนำคอร์สออนไลน์",
        video: "/",
        hasExample: true,
        hasDownload: false,
        hasTest: false,
        time: "01:07",
      },
    ],
  },
  {
    title: "ความสำคัญของ Design Thinking",
    session: [
      {
        title: "01. อะไรคือ Design Thinking?",
        video: "/",
        hasExample: true,
        hasDownload: true,
        hasTest: false,
        time: "22:40",
      },
      {
        title: "02. ความแตกต่างของ Design Thinking กับกระบวนการคิดรูปแบบอื่น",
        video: "/",
        hasExample: true,
        hasDownload: false,
        hasTest: false,
        time: "11:13",
      },
      {
        title: "03. ความเป็นมาของ Design Thinking",
        video: null,
        hasExample: false,
        hasDownload: false,
        hasTest: false,
        time: "11:13",
      },
    ],
  },
  {
    title: "องค์ประกอบของ Design Thinking",
    session: [
      {
        title: "04. ขั้นตอนของกระบวนการคิดเชิงออกแบบ: (Design Thinking Process) - Heart / Head / Hand",
        video: null,
        hasExample: false,
        hasDownload: false,
        hasTest: false,
        time: "10:10",
      }
    ],
  },
  {
    title: "แบบทดสอบที่ 1",
    session: [
      {
        title: "แบบทดสอบที่ 1 (5 คะแนน)",
        video: null,
        hasExample: false,
        hasDownload: false,
        hasTest: true,
        time: "แบบทดสอบ",
      }
    ],
  },
  {
    title: "HEART ทำความเข้าใจกลุ่มเป้าหมาย (Empathy)",
    session: [
      {
        title: "06. แนวคิดของการเข้าใจกลุ่มเป้าหมาย (Empathy)",
        video: null,
        hasExample: false,
        hasDownload: false,
        hasTest: true,
        time: "11:51",
      },
      {
        title: "07. ความแตกต่างของ Design Research กับ Market Research",
        video: null,
        hasExample: false,
        hasDownload: false,
        hasTest: true,
        time: "09:23",
      }
    ],
  }
];

const Home = () => {
  return (
    <Layout>
      {/*Start Banner*/}
      <Row className="banner-course-detail">
        <Container>
          <Row className="mx-xs-0 mx-2">
            <Col xs={12} lg={11}>
              <div className="banner-course-detail-sub-header">
                <span className="academic-icon mr-2">
                  <img src="https://skilllane.s3-ap-southeast-1.amazonaws.com/tu/icon-tuxsa.png" />
                </span>
                หลักสูตรปริญญาโทออนไลน์
              </div>

              <div className="banner-course-detail-header">
                <h2>Design Thinking กระบวนการคิดเชิงออกแบบ</h2>
              </div>

              <div className="banner-course-detail-degree btn btn-degree">
                <Link href="/">
                  ปริญญาโทบริหารธุรกิจ สาขา Business Innovation
                </Link>
              </div>

              <Row className="review-course-detail">
                <div className="order-xl-1 order-2">
                  <StarRatings
                    rating={4.7}
                    starRatedColor="#fdca19"
                    numberOfStars={5}
                    name="rating"
                    starDimension="22px"
                    starSpacing="2px"
                    style={{ marginBottom: "5px" }}
                  />
                </div>

                <div className="order-xl-2 order-3">
                  <h3 className="d-inline ml-2 pt-1 align-bottom">
                    4.7 (607 รีวิว)
                  </h3>
                </div>

                <div className="order-xl-3 order-1">
                  <Button className="btn-share ml-3 py-0 order-1">
                    <span className="btn-share-icon">
                      <FontAwesomeIcon
                        icon={faShareAlt}
                      />
                    </span>
                    <span className="btn-share-text ml-1">แชร์คอร์สนี้</span>
                  </Button>
                </div>
              </Row>
            </Col>
          </Row>
        </Container>
      </Row>
      {/*End Banner*/}

      {/*Start content course*/}
      <div className="content-course">
        <Container>
          <Row className="content-course-row">
            <Col xl={6} lg={12}>
              <img
                className="img-fluid"
                src="https://resource.skilllane.com/courses/previews/000/001/632/large/640x360_designthinking.jpg"
              />
            </Col>

            {/*not save*/}
            <Col xl={3} lg={12} className="mt-sm-0 mt-1">
              <Row className="glance-content mr-sm-0 my-sm-0 m-1 border border-success">
                <Col xs={12} md={6} lg={12}>
                  <h3>1,500 บาท</h3>
                </Col>

                <Col xs={12} md={6} lg={12} className="glance-header">
                  <Button className="btn btn-enroll">
                    ชำระเงินเรียนไม่เก็บหน่วยกิต
                  </Button>
                </Col>

                <Col xs={12} md={6} lg={12} className="mt-2">
                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faCheck} />
                    </div>
                    <div className="glance-item-text">
                      <p>สามารถเรียนที่ไหน เมื่อไหร่ก็ได้ตลอดชีพ</p>
                    </div>
                  </div>

                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faCheck} />
                    </div>
                    <div className="glance-item-text">
                      <p>
                        เนื้อหาทั้งหมด 40 วิดีโอ ความยาวรวมกัน 7 ชั่วโมง 46 นาที
                      </p>
                    </div>
                  </div>

                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faCheck} />
                    </div>
                    <div className="glance-item-text">
                      <p>เอกสารประกอบ</p>
                    </div>
                  </div>

                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faTimes} />
                    </div>
                    <div className="glance-item-text">
                      <p>แบบทดสอบทั้งหมด 4 แบบทดสอบ</p>
                    </div>
                  </div>

                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faTimes} />
                    </div>
                    <div className="glance-item-text">
                      <p>ข้อสอบทั้งหมด 1 ข้อสอบ</p>
                    </div>
                  </div>

                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faTimes} />
                    </div>
                    <div className="glance-item-text">
                      <p>เก็บหน่วยกิตเรียนปริญญาโท</p>
                    </div>
                  </div>

                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faTimes} />
                    </div>
                    <div className="glance-item-text">
                      <p>ประกาศนียบัตร</p>
                    </div>
                  </div>

                </Col>
              </Row>
            </Col>

            {/*save*/}
            <Col xl={3} lg={12} xs={12} className="mt-sm-0 mt-1">
              <Row className="glance-content mr-sm-0 my-sm-0 m-1 border border-success">
                <Col xs={12} md={6} lg={12} className="glance-pay">
                  <h3>4,500 บาท</h3>
                </Col>

                <Col xs={12} md={6} lg={12} className="glance-header">
                  <Button className="btn btn-enroll active">
                    ชำระเงินเรียนเก็บหน่วยกิต
                  </Button>
                </Col>

                <Col xs={12} md={6} lg={12} className="mt-2">
                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faCheck} />
                    </div>
                    <div className="glance-item-text">
                      <p>สามารถเรียนที่ไหน เมื่อไหร่ก็ได้ตลอดชีพ</p>
                    </div>
                  </div>

                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faCheck} />
                    </div>
                    <div className="glance-item-text">
                      <p>
                        เนื้อหาทั้งหมด 40 วิดีโอ ความยาวรวมกัน 7 ชั่วโมง 46 นาที
                      </p>
                    </div>
                  </div>

                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faCheck} />
                    </div>
                    <div className="glance-item-text">
                      <p>เอกสารประกอบ</p>
                    </div>
                  </div>

                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faCheck} />
                    </div>
                    <div className="glance-item-text">
                      <p>แบบทดสอบทั้งหมด 4 แบบทดสอบ</p>
                    </div>
                  </div>

                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faCheck} />
                    </div>
                    <div className="glance-item-text">
                      <p>ข้อสอบทั้งหมด 1 ข้อสอบ</p>
                    </div>
                  </div>

                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faCheck} />
                    </div>
                    <div className="glance-item-text">
                      <p>เก็บหน่วยกิตเรียนปริญญาโท</p>
                    </div>
                  </div>

                  <div className="glance-item">
                    <div className="glance-item-icon">
                      <FontAwesomeIcon icon={faCheck} />
                    </div>
                    <div className="glance-item-text">
                      <p>ประกาศนียบัตร</p>
                    </div>
                  </div>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </div>
      {/*End content course*/}
      <Container>
        <Row>
          <Col xs={12} lg={9}>
            <DetailsCourse />

            <div>
              <h1>เนื้อหาของคอร์สนี้</h1>
              <div className="boxCourse">
              {
                sessionCourse.map((list) => {
                  return (
                      <Session data={list} />
                  )
                })
              }
              </div>
            </div>
          </Col>

          <Col xs={12} lg={3} className="sidebar d-md-block d-none">
            <h4 className="text-green title">ผู้สอน</h4>
            <div className="instructor-card -border" wfd-id="94">
              <div className="instructor-card-head" wfd-id="102">
                <div className="instructor-profile-image" wfd-id="106">
                  <img alt="เมษ์ ศรีพัฒนาสกุล"
                       src="https://resource.skilllane.com/users/images/001/162/361/ms/Untitled.png?1566569677" />
                </div>
                <div className="instructor-profile-title" wfd-id="103">
                  <div className="profile-title-name" wfd-id="105">
                    <Link href="https://www.skilllane.com/instructors/may-sripatanaskul">
                      เมษ์ ศรีพัฒนาสกุล
                    </Link>
                  </div>
                  <div className="profile-rating" wfd-id="104">
                    <p>
                      <svg className="svg-inline--fa fa-star fa-w-18 mr-2" aria-hidden="true" focusable="false"
                           data-prefix="fa" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg"
                           viewBox="0 0 576 512" data-fa-i2svg="">
                        <path fill="currentColor"
                              d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z"></path>
                      </svg>
                      4.7 คะแนนเฉลี่ย
                    </p>
                    <p>
                      <svg className="svg-inline--fa fa-comment fa-w-16 mr-2" aria-hidden="true" focusable="false"
                           data-prefix="fa" data-icon="comment" role="img" xmlns="http://www.w3.org/2000/svg"
                           viewBox="0 0 512 512" data-fa-i2svg="">
                        <path fill="currentColor"
                              d="M256 32C114.6 32 0 125.1 0 240c0 49.6 21.4 95 57 130.7C44.5 421.1 2.7 466 2.2 466.5c-2.2 2.3-2.8 5.7-1.5 8.7S4.8 480 8 480c66.3 0 116-31.8 140.6-51.4 32.7 12.3 69 19.4 107.4 19.4 141.4 0 256-93.1 256-208S397.4 32 256 32z"></path>
                      </svg>
                      610 รีวิว
                    </p>
                    <p>
                      <svg className="svg-inline--fa fa-play-circle fa-w-16 mr-2" aria-hidden="true" focusable="false"
                           data-prefix="fa" data-icon="play-circle" role="img" xmlns="http://www.w3.org/2000/svg"
                           viewBox="0 0 512 512" data-fa-i2svg="">
                        <path fill="currentColor"
                              d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm115.7 272l-176 101c-15.8 8.8-35.7-2.5-35.7-21V152c0-18.4 19.8-29.8 35.7-21l176 107c16.4 9.2 16.4 32.9 0 42z"></path>
                      </svg>
                      1 คอร์ส
                    </p>
                  </div>
                </div>
              </div>
              <div className="instructor-card-content" wfd-id="95">
                <ul className="instructor-card-detail" wfd-id="96">
                  <li wfd-id="101">กรรมการผู้จัดการและผู้ร่วมก่อตั้ง Asian Leadership Academy / LUKKID
                  </li>
                  <li wfd-id="100">ที่ปรึกษาเรื่องการคิดเชิงออกแบบ (Design Thinking) ให้กับบริษัทชั้นนำของประเทศไทย
                  </li>
                  <li wfd-id="99">หัวหน้าทีมคิดเชิงออกแบบที่ Hasso Plattner Institute of Design ที่ Stanford (d.school)
                  </li>
                  <li wfd-id="98">จบการศึกษาปริญญาโทสาขา MBA จาก Stanford Graduate School of Business
                  </li>
                  <li wfd-id="97">จบการศึกษาปริญญาตรีสาขา Bioengineering จาก University of Pennsylvania</li>
                </ul>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </Layout>
  );
};

export default Home;
